
public class MainRun {

	public static void main(String[] args) {
		Library lbr = new Library();

		Teacher kru = new Teacher("Sirikorn", "Junnuan", "D14", "0012");
		Student stu = new Student("Nitchakarn", "Jinanukulwong", "D14",
				"5610404371");
		Worker wk = new Worker("Somsri", "Sudsuay");

		System.out.println(lbr.checkBook());
		
		System.out.println(lbr.borrowBook(kru,"Java"));
		System.out.println(kru.getDetails());
		lbr.returnBook(kru,"Java");
		System.out.println(lbr.checkBook());
		System.out.println(kru.getDetails());
		System.out.println("------------------------------------");
		
		System.out.println(lbr.borrowBook(stu,"Database"));
		System.out.println(stu.getDetails());
		lbr.returnBook(stu,"Database");
		System.out.println(lbr.checkBook());
		System.out.println(stu.getDetails());
		System.out.println("------------------------------------");
		
		System.out.println(lbr.borrowBook(wk,"Reference Book"));
		System.out.println(wk.getDetails());
		
		System.out.println(lbr.checkBook());

	}

}
