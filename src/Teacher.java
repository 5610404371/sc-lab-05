
public class Teacher {
	String name;
	String lastname;
	String major;
	String id;
	String bookName;

	public Teacher(String name, String lastname, String major, String id) {
		this.name = name;
		this.lastname = lastname;
		this.major = major;
		this.id = id;

	}
	public String getid(){
		 return id;
		 }
	 public void setid(String id){
		 this.id = id;
		  }
	 public String getName(){
		 return name;
		  }
	 public void setLastName(String lastname){
		 this.lastname = lastname;
		  }
	 public String getLastName(){
		 return lastname;
		  }
	 public void setName(String name){
		 this.name = name;
		  }
	 public String getMajor(){
		 return major;
		  }
	 public void setMajor(String major){
		 this.major = major;
		  }
	 
	 public String getBookName(){
		 return bookName;
		  }
	 public void setBookName(String bookName){
		 this.bookName = bookName;
	 }
	 

	 public String getDetails(){
	    return (name +" "+lastname+" "+major+" "+id+" "+bookName);
	    }


}
