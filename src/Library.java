import java.util.ArrayList;


public class Library {
	String bookName;
	ArrayList<String> listBook = new ArrayList<String>();
	String t = "You can borrow this book.";
	String f = "You can't borrow this book.";
	String o = "This book has already borrowed.";
	public Library(){
		listBook.add("Java");
		listBook.add("Database");
		listBook.add("Linear");
		listBook.add("Alogorithm");
		listBook.add("Reference Book");

	}

	public String borrowBook(Teacher kru, String bookName) {

		if (bookName.equals("Reference Book")) {
			kru.setBookName(bookName);
			return f;
		} else if (listBook.contains(bookName)) {
			listBook.remove(bookName);
				kru.setBookName(bookName);
			return t;
		} else {
			return o;
		}
	}
	
	public String borrowBook(Student stu, String bookName) {

		if (bookName.equals("Reference Book")) {
			stu.setBookName(bookName);
			return f;
		} else if (listBook.contains(bookName)) {
			listBook.remove(bookName);
				stu.setBookName(bookName);
			return t;
		} else {
			return o;
		}
	}
	
	public String borrowBook(Worker wk, String bookName) {

		if (bookName.equals("Reference Book")) {
			wk.setBookName(bookName);
			return f;
		} else if (listBook.contains(bookName)) {
			listBook.remove(bookName);
				wk.setBookName(bookName);
			return t;
		} else {
			return o;
		}
	}
	 
	 public void returnBook(Teacher kru,String bookName){
		 listBook.add(bookName);
		 kru.setBookName("");
	 }
	 
	 public void returnBook(Student stu,String bookName){
		 listBook.add(bookName);
		 stu.setBookName("");
	 }
	 
	 public void returnBook(Worker wk,String bookName){
		 listBook.add(bookName);
		 wk.setBookName("");
	 }
	 
	 public ArrayList<String> checkBook(){
		 return listBook;
	 }
	 
	 
	 }
