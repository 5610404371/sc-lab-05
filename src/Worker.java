
public class Worker {
	String name;
	String lastname;
	String bookName;

	public Worker(String name, String lastname) {
		this.name = name;
		this.lastname = lastname;

	}
	 public String getBookName(){
		 return bookName;
		  }
	 public void setBookName(String bookName){
		 this.bookName = bookName;
	 }

	public String getDetails() {
		return (name + " " + lastname+" "+bookName);
	}

}
